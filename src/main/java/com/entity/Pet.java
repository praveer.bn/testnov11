package com.entity;

import javax.persistence.*;
import java.util.List;
@Entity
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private  int pid;
    private String pName;
    private String pType;
    @OneToMany(mappedBy = "pet")
    List<Booking> bookingList;

    public Pet() {
    }

    public Pet(String pName, String pType) {
        this.pName = pName;
        this.pType = pType;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "pid=" + pid +
                ", pName='" + pName + '\'' +
                ", pType='" + pType + '\'' +
                ", bookingList=" + bookingList +
                '}';
    }
}
