package com.entity;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;

@Entity
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private  int did;
    private String dName;
    private String dType;
    private int available;
    private  int fee;
    private Time stime;
    private Time etime;
    private  long phNo;
    @OneToMany(mappedBy = "doctor")
    List<Booking> bookingList;

    public Doctor() {
    }

    public Doctor(String dName, String dType, int available, int fee, Time stime, Time etime, long phNo) {
        this.dName = dName;
        this.dType = dType;
        this.available = available;
        this.fee = fee;
        this.stime = stime;
        this.etime = etime;
        this.phNo = phNo;
    }

    public Time getStime() {
        return stime;
    }

    public void setStime(Time stime) {
        this.stime = stime;
    }

    public Time getEtime() {
        return etime;
    }

    public void setEtime(Time etime) {
        this.etime = etime;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public String getdType() {
        return dType;
    }

    public void setdType(String dType) {
        this.dType = dType;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public long getPhNo() {
        return phNo;
    }

    public void setPhNo(long phNo) {
        this.phNo = phNo;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "did=" + did +
                ", dName='" + dName + '\'' +
                ", dType='" + dType + '\'' +
                ", available=" + available +
                ", fee=" + fee +
                ", stime=" + stime +
                ", etime=" + etime +
                ", phNo=" + phNo +
                ", bookingList=" + bookingList +
                '}';
    }
}
