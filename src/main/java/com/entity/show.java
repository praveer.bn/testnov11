package com.entity;

import com.dao.BookingDaoImpl;
import com.dao.DoctorDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class show extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
resp.setContentType("text/html");
        PrintWriter out=resp.getWriter();
        DoctorDaoImpl dao=new DoctorDaoImpl();
        List<Doctor> list=dao.show();
        out.print("<table border=2px>");
        out.print("<th>Doctor NAME</th><th>doctor id</th><th>doctor number </th><th>doctor slot</th>");
        for (Doctor s : list) {
           out.println("<tr><td>" + s.getdName() + "</td><td>" + s.getDid() + "</td><td>" + s.getPhNo() + "</td><td>" + s.getAvailable() + " </td></tr>");
        }

    }
}
//<%
//        response.setContentType("text/html");
//
//        DoctorDaoImpl dao=new DoctorDaoImpl();
//        List<Doctor> list=dao.show();
//        out.print("<table border=2px>");
//        out.print("<th>Doctor NAME</th><th>doctor id</th><th>doctor number </th><th>doctor slot</th>");
//        for (Doctor s : list) {
//        out.println("<tr><td>" + s.getdName() + "</td><td>" + s.getDid() + "</td><td>" + s.getPhNo() + "</td><td>" + s.getAvailable() + " </td></tr>");
//        }
//        %>