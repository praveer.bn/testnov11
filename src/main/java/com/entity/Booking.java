package com.entity;

import javax.persistence.*;
import java.sql.Time;

@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int bid;
    private String appointmentStatus;
    private String paymentStatus;
    @ManyToOne
    private Doctor doctor;
    private Time psTime;
    private Time peTime;
    @ManyToOne
    private Pet pet;

    public Booking() {
    }

    public Booking(String appointmentStatus, String paymentStatus, Doctor doctor, Pet pet) {
        this.appointmentStatus = appointmentStatus;
        this.paymentStatus = paymentStatus;
        this.doctor = doctor;
        this.pet = pet;
    }

    public Booking(String appointmentStatus, String paymentStatus, Doctor doctor, Time psTime, Time peTime, Pet pet) {
        this.appointmentStatus = appointmentStatus;
        this.paymentStatus = paymentStatus;
        this.doctor = doctor;
        this.psTime = psTime;
        this.peTime = peTime;
        this.pet = pet;
    }

    public Time getPsTime() {
        return psTime;
    }

    public void setPsTime(Time psTime) {
        this.psTime = psTime;
    }

    public Time getPeTime() {
        return peTime;
    }

    public void setPeTime(Time peTime) {
        this.peTime = peTime;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bid=" + bid +
                ", appointmentStatus='" + appointmentStatus + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", doctor=" + doctor +
                ", psTime=" + psTime +
                ", peTime=" + peTime +
                ", pet=" + pet +
                '}';
    }
}
