package com.servlet;

import com.dao.PetDaoImpl;
import com.entity.Pet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/addPet")
public class AddPet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name=req.getParameter("pName");
        String type= req.getParameter("type");
        PetDaoImpl petDao=new PetDaoImpl();
        Pet pet=new Pet(name,type);
        petDao.addPet(pet);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
        printWriter.print("Pet Added");
        requestDispatcher.include(req, resp);
    }
}
