package com.servlet;

import com.dao.DoctorDaoImpl;
import com.entity.Doctor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;

@WebServlet("/addDoc")
public class AddDoctor extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name= req.getParameter("dName");
        String type= req.getParameter("type");
        int slot= Integer.parseInt(req.getParameter("slot"));
        int fee= Integer.parseInt(req.getParameter("fee"));
        long ph= Long.parseLong(req.getParameter("ph"));
        Time stime= Time.valueOf(req.getParameter("stime")+":00");
        Time etime= Time.valueOf(req.getParameter("etime")+":00");
        Doctor doctor=new Doctor(name,type,slot,fee,stime,etime,ph );
        DoctorDaoImpl doctorDao=new DoctorDaoImpl();
        doctorDao.addDoctor(doctor);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
        printWriter.print("Doctor Added");
        requestDispatcher.include(req, resp);

    }
}
