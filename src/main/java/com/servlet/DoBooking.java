package com.servlet;

import com.connection.JPAprovider;
import com.dao.BookingDaoImpl;
import com.entity.Booking;
import com.entity.Doctor;
import com.entity.Pet;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;

@WebServlet("/addBook")
public class DoBooking extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();

        int did= Integer.parseInt(req.getParameter("did"));
        int pid= Integer.parseInt(req.getParameter("pid"));
        int fee= Integer.parseInt(req.getParameter("fee"));
        long ph= Long.parseLong(req.getParameter("ph"));

        Time petstime= Time.valueOf(req.getParameter("stime")+":00");
        Time petetime= Time.valueOf(req.getParameter("etime")+":00");

        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        Doctor doctor=entityManager.find(Doctor.class,did);
        Pet pet=entityManager.find(Pet.class,pid);
        if(doctor.getdType().equals(pet.getpType())){
            if (doctor.getAvailable()>0){
                if (petstime.after(doctor.getStime()) && petetime.before(doctor.getEtime())) {
                    if (doctor.getFee() == fee) {
                        if (doctor.getPhNo() == ph) {
                            entityManager.getTransaction().begin();
                            doctor.setAvailable(doctor.getAvailable() - 1);
                            entityManager.persist(doctor);
                            entityManager.getTransaction().commit();
                            Booking booking = new Booking("booked", "done",doctor,petstime,petetime,pet);
                            BookingDaoImpl bookingDao = new BookingDaoImpl();
                            bookingDao.doBooking(booking);
                            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                            printWriter.print("Booking And Payment Done Successfully And SEE YOU IN HOSPITAL");
                            requestDispatcher.include(req, resp);

                        } else {
                            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                            printWriter.print("Wrong Doctor NUmber For Online Payment ");
                            requestDispatcher.include(req, resp);
                        }

                    } else {
                        RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                        printWriter.print("Plz enter proper Fee Amonut ");
                        requestDispatcher.include(req, resp);
                    }
                }else {
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                    printWriter.print("out of time");
                    requestDispatcher.include(req, resp);
                }
            }else{
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("Doctor is Not Available Today...Plz Try Tomorrow ");
                requestDispatcher.include(req, resp);
            }

        }else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            printWriter.print("Doctor Specification Type and Pet Type Is Different ");
            requestDispatcher.include(req, resp);
        }

    }
}
