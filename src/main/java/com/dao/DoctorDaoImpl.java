package com.dao;

import com.connection.JPAprovider;
import com.entity.Doctor;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class DoctorDaoImpl implements DoctorDao{
    public void addDoctor(Doctor doctor) {
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(doctor);
            entityManager.getTransaction().commit();
            entityManager.close();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }
    }

    public List<Doctor> show() {
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Doctor> criteriaQuery=criteriaBuilder.createQuery(Doctor.class);
        Root<Doctor> from=criteriaQuery.from(Doctor.class);
        CriteriaQuery<Doctor> select=criteriaQuery.select(from);
        TypedQuery<Doctor> typedQuery=entityManager.createQuery(select);
        List<Doctor> list=typedQuery.getResultList();
        return list;
    }
}
