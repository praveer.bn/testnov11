package com.dao;

import com.entity.Doctor;

import java.util.List;

interface DoctorDao {
     void addDoctor(Doctor doctor);
     List<Doctor> show();
}
