package com.dao;

import com.connection.JPAprovider;
import com.entity.Pet;

import javax.persistence.EntityManager;

public class PetDaoImpl implements PetDao{
    public void addPet(Pet pet) {
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(pet);
            entityManager.getTransaction().commit();
            entityManager.close();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }
    }
}
