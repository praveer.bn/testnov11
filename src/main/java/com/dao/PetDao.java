package com.dao;

import com.entity.Pet;

interface PetDao {
     void addPet(Pet pet);
}
