package com.dao;

import com.entity.Booking;
import com.entity.Doctor;
import com.entity.Pet;

interface BookingDao {
    void doBooking(Booking booking);
//    , Doctor doctor, Pet pet
}
