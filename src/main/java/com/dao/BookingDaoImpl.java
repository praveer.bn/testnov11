package com.dao;

import com.connection.JPAprovider;
import com.entity.Booking;
import com.entity.Doctor;
import com.entity.Pet;

import javax.persistence.EntityManager;

public class BookingDaoImpl implements BookingDao{
    public void doBooking(Booking booking) {
        EntityManager entityManager= JPAprovider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        try {
//            entityManager.persist(pet);
//            entityManager.persist(doctor);
            entityManager.persist(booking);
            entityManager.getTransaction().commit();
            entityManager.close();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }
    }
}
